<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Inicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('nacimiento');
            $table->string('apellido');
            $table->string('nombre');
            $table->timestamps();
        });
        Schema::create('tipo_alimentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('codigo');
            $table->string('descripcion');
            $table->timestamps();
        });
        Schema::create('porcions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('codigo');
            $table->string('descripcion');
            $table->timestamps();
        });
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_inicio');
            $table->unsignedBigInteger('persona_id');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->timestamps();
        });
        Schema::create('alimentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('codigo');
            $table->string('descripcion');
            $table->unsignedBigInteger('tipoalimento_id');
            $table->foreign('tipoalimento_id')->references('id')->on('tipo_alimentos');
            $table->timestamps();
        });
        Schema::create('porciones_x_alimentos', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('alimento_id');
            $table->foreign('alimento_id')->references('id')->on('alimentos');
            $table->unsignedBigInteger('porcion_id');
            $table->foreign('porcion_id')->references('id')->on('porcions');
            $table->decimal('gramos', 8, 2);
        });
        Schema::create('dias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha');
            $table->timestamps();
        });
        Schema::create('comidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('codigo');
            $table->string('descripcion');
            $table->unsignedBigInteger('porcion_x_alimento_id');
            $table->foreign('porcion_x_alimento_id')->references('id')->on('porciones_x_alimentos');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

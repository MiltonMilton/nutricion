<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('/tipo_alimentos', 'TipoAlimentoController@index' );
Route::get('/tipo_alimentos/create', 'TipoAlimentoController@create');
Route::post('/tipo_alimentos', 'TipoAlimentoController@store' );
Route::get('/alimentos/create', 'AlimentoController@create' );



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoAlimento;

class TipoAlimentoController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            return TipoAlimento::all();
        }else{
            return view('tipo_alimento.index');
        }
    }
    public function create(){
        return view('tipo_alimento.crear');
    }

    public function store(Request $request){

        $tipo_alimento = new TipoAlimento();
        $tipo_alimento->descripcion = $request->descripcion;
        $tipo_alimento->codigo = $request->codigo;
        $tipo_alimento->save();

        return $tipo_alimento;
    }
}
